# Generated by Django 3.2.8 on 2021-11-24 20:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_alter_proceso_vistas'),
    ]

    operations = [
        migrations.AlterField(
            model_name='individuo',
            name='vistas',
            field=models.IntegerField(default=0),
        ),
    ]

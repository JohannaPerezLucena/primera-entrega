from django.db import models

from django.contrib.auth.models import User

# Create your models here.
class Partido(models.Model):
    nombre = models.CharField(max_length=45, null=False)
    vistas = models.IntegerField(default=0)
    creador = models.ForeignKey(
        User,
        on_delete=models.PROTECT
    )

    class Meta:
        app_label: 'app'

class Individuo(models.Model):
    apellidos = models.CharField(max_length=45, null=False)
    nombres = models.CharField(max_length=45, null=False)
    fecha_nacimiento = models.DateField(auto_now=False, auto_now_add=False, null=True)
    aprobado = models.BooleanField(null=True)
    vistas = models.IntegerField(default=0)
    creador = models.ForeignKey(
        User,
        on_delete=models.PROTECT
    )

     

    class Meta:
        app_label: 'app'

class Afiliacion(models.Model):
    id_individuo = models.ForeignKey(
        Individuo,
        related_name='id_individuos',
        on_delete=models.PROTECT
    )
    id_partido = models.ForeignKey(
        Partido,
        related_name='id_partidos',
        on_delete=models.PROTECT
    )
    fecha_ingreso = models.DateField(auto_now=False, auto_now_add=False, null=True)
    fecha_salida = models.DateField(auto_now=False, auto_now_add=False, null=True)
    aprobado = models.BooleanField(null=True)

    	
 


    class Meta:
        app_label: 'app'

class Proceso(models.Model):
    titulo = models.CharField(max_length=45, null=False)
    fecha_inicio = models.DateField(auto_now=False, auto_now_add=False, null=False)
    fecha_fin = models.DateField(auto_now=False, auto_now_add=False, null=True)
    abierto = models.BooleanField(null=False)
    entidad = models.CharField(max_length=45, null=False)
    monto = models.IntegerField(null=True)
    comentarios = models.CharField(max_length=45, null=False)
    aprobado = models.BooleanField(null=True)
    vistas = models.IntegerField(default=0)
    creador = models.ForeignKey(
        User,
        on_delete=models.PROTECT
    )

    class Meta:
        app_label: 'app'


class Implicado(models.Model):
    id_afiliado = models.ForeignKey(
        Afiliacion,
        on_delete=models.PROTECT
    )
    id_proceso = models.ForeignKey(
        Proceso,
        on_delete=models.PROTECT
    )
    fecha = models.DateField(auto_now=False, auto_now_add=False)
    acusacion = models.CharField(max_length=45, null=False)
    culpable = models.BooleanField(null=True)
    pena = models.CharField(max_length=45, null=False)
    comentarios = models.CharField(max_length=45, null=False)

    class Meta:
        app_label: 'app'
from django.db.models.fields.related import OneToOneField
from django.db.models.query_utils import Q
from django.shortcuts import render, redirect, resolve_url	
from django.http import HttpResponse
from django.http.response import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from app.models import Implicado, Partido, Individuo, Proceso
from app.models import Afiliacion, Partido, Individuo, Proceso
from operator import itemgetter


# from app.models import username
# Create your views here.

def principal_view(request):
    return render(request, 'app/html/principal.html')

def inicio_view(request):
    if(request.user.is_authenticated):
        if(request.user.is_superuser):
            lista= Partido.objects.all()
            print(lista)
            contextos={
                'listapartido':lista
            }
            return render(request, 'app/html/inicio.html',contextos)
        else:
            return redirect('app:inicio_ciudadano')
    else:
        return redirect('app:principal')
    
def inicio_ciudadano_view(request):
    
    if(request.user.is_authenticated):
        if(request.user.is_superuser):
            return render(request, 'app/html/inicio.html')
        else:
            lista= Partido.objects.all()
            print(lista)
            contextos={
                'listapartido':lista
            }

            return render(request,'app/html/inicio_ciudadano.html',contextos)
    else:
        return redirect('app:principal')



    

def cambiar_datos_view(request):
    return render (request, 'app/html/cambiar_datos.html')

def registro_view(request):
    return render(request, 'app/html/registro.html')

def iniciar_sesion_view(request):
    return render(request, 'app/html/iniciar_sesion.html')

def cerrar_sesion_view(request):
    logout(request)
    return redirect('app:principal')

def form_iniciar_sesion_view(request):
    usernamePost = request.POST['usuario']
    password = request.POST['contraseña']
    try:
        user = User.objects.get(username=usernamePost, is_active=True)
        if(authenticate(username=usernamePost, password=password) != None):
            login(request, user)
            return redirect('app:inicio')
        else:
            return render(request, 'app/html/iniciar_sesion_error.html')
    except:
        print('for error')
        return render(request, 'app/html/iniciar_sesion_error.html')
    

def crear_partido_view (request):
    return render(request,'app/html/crear_partido.html' )

def form_crear_partido(request):
    if(request.user.is_authenticated):
        name = request.POST['nombre']
        creator = request.user
        partido = Partido(nombre=name, creador=creator)
        partido.save()
        return redirect('app:inicio')
    else:
        return redirect('app:principal')
    

def crear_individuo_view(request):
    return render(request,'app/html/crear_individuo.html')

def form_crear_individuo_view(request):
    if(request.user.is_authenticated):
        name = request.POST['usuario']
        lastname = request.POST['Apellido']
        date = request.POST['date']
        creator = request.user
        if(request.user.is_superuser):
            individuo = Individuo(apellidos=lastname, nombres=name, fecha_nacimiento=date, aprobado=True, creador=creator)
            individuo.save()
        else:
            individuo = Individuo(apellidos=lastname, nombres=name, fecha_nacimiento=date, aprobado=False, creador=creator)
            individuo.save()
        return redirect('app:inicio')
    else:
        return redirect('app:principal')

def crear_proceso_view(request):
    return render(request,'app/html/crearProceso.html')

def form_crear_proceso_view(request):
    if(request.user.is_authenticated):
        title = request.POST['title']
        startDate = request.POST['startDate']
        endDate = request.POST['endDate']
        state = request.POST['state']
        entidad = request.POST['entidad']
        monto = request.POST['monto']
        comentario = request.POST['comentarios']
        creator = request.user
        if(request.user.is_superuser):
            proceso = Proceso(titulo=title, fecha_inicio=startDate, fecha_fin=(endDate if endDate != "" else None), abierto=state, entidad=entidad, monto=(monto if monto != "" else None), comentarios=(comentario if comentario != "" else None), aprobado=True, creador=creator)
            proceso.save()
        else:
            proceso = Proceso(titulo=title, fecha_inicio=startDate, fecha_fin=(endDate if endDate != "" else None), abierto=state, entidad=entidad, monto=(monto if monto != "" else None), comentarios=(comentario if comentario != "" else None), aprobado=False, creador=creator)
            proceso.save()
        return redirect('app:inicio')
    else:
        return redirect('app:principal')

def consultar_lista_partido_view(request):



    lista= Partido.objects.all()
    print(lista)

    procesos=[]
    contador=0
    lista2=[]
    
    for i in lista:
        afiliados=Afiliacion.objects.filter(id_partido=i.id, aprobado =True)
        
        procesos.append(0)
        print("prueba: ",i )
        for j in afiliados:
            par=Implicado.objects.filter(id_afiliado=j.id)
            procesos[contador]+=len(par)
            
            print("afiliados: ",j.id)
            print("partido: ",par)
        
        lista2.append({ 'partido':i,
        'procesos':procesos[contador]})
        
        lista3 =sorted(lista2, key=lambda d: d['procesos'], reverse=True)
        print("lista3: ",lista3)
        contador+=1
        

    print("lista2: ",lista2 )     
    print("Procesos: ", procesos)    
    # lista2={
    #     'partidos':lista,
    #     'procesos':procesos
    # }

    # oe4= Afiliacion.objects.filter(Q(id_partido_id=infopartido) & Q(aprobado=True)).values_list('id',flat=True)

    
    # print("haber",par)

    contextos={
        'listapartido':lista3,

    }

    return render(request,'app/html/consultar_lista_partido.html',contextos)

def consultar_individuo_view(request):
    lista= Individuo.objects.all()
    
    contexto={
        'individuos':lista
        
    }  

    return render (request,'app/html/consultarindividuoprev.html', contexto)

def consultar_individuo_form(request):
    ind = int(request.POST['elegirp'])
    personas= Individuo.objects.get(id=ind)
    lista= Afiliacion.objects.filter(id_individuo_id=ind)
    individuos= Individuo.objects.all()
    # partidos= Partido.objects.get(id=lista.id_partido_id)
    
    personas.vistas +=1
    personas.save()

    afiliaciones=Afiliacion.objects.filter(id_individuo=ind)   
    
    print("Afiliacion", lista)
    procesos= []
    for i in lista:
        impli=Implicado.objects.filter(id_afiliado_id=i.id)
        procesos += impli

    print("proceso", procesos)
    contexto = {
        'persona':personas,
        'lista':lista,
        'implicacion':procesos,
        'implic':impli,
        'individuos':individuos
    }

    return render (request, 'app/html/consultarindividuoprev.html', contexto)

def consultar_partido_view(request):
    lista= Partido.objects.all()
    print("blaaa ",lista)
    
    contexto={
        'listapartidos':lista
    }
    
    return render(request,'app/html/consultar_partido.html',contexto)

def consultar_partido_form(request):

    infopartido = int(request.POST['elegirp'])   
    listapartidos = Partido.objects.all()
    partido = Partido.objects.get(id=infopartido)    
    partido.vistas +=1
    partido.save()
    print("holis: ", partido)    
    oe= Afiliacion.objects.filter(Q(id_partido_id=infopartido) & Q(aprobado=True)).values_list('id_individuo_id',flat=True)
    oe3= Afiliacion.objects.filter(Q(id_partido_id=infopartido) & Q(aprobado=True))
    oe32 =sorted(oe3, key=lambda d: d.id_individuo.apellidos)
    oe4= Afiliacion.objects.filter(Q(id_partido_id=infopartido) & Q(aprobado=True)).values_list('id',flat=True)
    oe2= Afiliacion.objects.filter(Q(id_partido_id=infopartido) & Q(aprobado=True)).values().exists()
    # lista=0
    print("esto: ", oe3 )
    if oe2 == True:
        for i in range(0,len(oe)):
                lista = Individuo.objects.filter(id=oe[0]).order_by('apellidos').values()
                lista |= Individuo.objects.filter(id=oe[i]).order_by('apellidos').values()
                procesost=Implicado.objects.filter(id_afiliado_id=oe4[i]).count()
                procesosc=Implicado.objects.filter(Q(id_afiliado_id=oe4[i]) & Q(culpable=True)).count()
    else:
        lista=[]
        procesost=[]
        procesosc=[]
    

    print("sii: ", lista)
    # procesost=Implicado.objects.filter().count()
    # print("cantidad: ", procesost)
    contexto={
        'nombrepartido':partido,
        'lista':lista,
        'info':oe32,
        'procesost':procesost,
        'procesosc':procesosc,
        'listapartidos':listapartidos
    }    

    return render(request,'app/html/consultar_partido.html',contexto)
    
    
def consultar_lista_view(request):
    lista= Individuo.objects.all()
    lista2= Proceso.objects.all()
    lista3= Afiliacion.objects.all()

    individuos=[]
    for individuo in lista:
        afiliaciones= Afiliacion.objects.filter(id_individuo=individuo.id)
        print("individuo",individuo)
        procesos=0
        for afiliado in afiliaciones:
            valor=Implicado.objects.filter(id_afiliado=afiliado.id)
            procesos += len(valor)

        
        if len(afiliaciones)>0:
            afiliaciones  = afiliaciones[len(afiliaciones)-1]
            print("afiliaciones",afiliaciones)
            individuos.append({'individuo':individuo,'partido':afiliaciones.id_partido.nombre,'procesos':procesos})
            
        else :
            individuos.append({'individuo':individuo,'partido':'Sin partido','procesos':procesos})
   

    print("buenas",lista)
    contexto={
        'individuos':individuos,
        'proceso':lista2,
        'afiliacion':lista3,
        
    }
    return render (request,'app/html/consultar_lista.html', contexto)

def consultar_lista_post (request):


    return render (request, 'app/html/consultar_lista.html')



def consultar_proceso_view(request):
    procesos = Proceso.objects.all()
    contexto = {
        'procesos':procesos
    }
    return render (request,'app/html/consultarprocesoprev.html', contexto )

def consultar_proceso_individual_view(request):
    id = request.POST['id']
    proceso = Proceso.objects.get(id=id)
    proceso.vistas += 1
    
    proceso.save()
    implicados = Implicado.objects.filter(id_proceso=id)
    contexto = {
        'proceso':proceso,
        'implicados':implicados
    }
    return render (request,'app/html/ConsultarProceso.html', contexto)

def aprobar_individuo_view(request):
    lista= Individuo.objects.all()
    contexto={
        'individuos':lista
        
    }
    return render(request,'app/html/aprobar_individuo.html',contexto )

def aprobar_individuo_post(request):

    info1= request.POST['seleccionar_ind']
    # print(info1)
    cat1 = Individuo.objects.get(id=info1)
    info2= int (request.POST['seleccionar'])
    print(info2)
    print(cat1.nombres)
    if info2 == 10:        
        cat1.aprobado= True
        cat1.save()
    else:
        cat1.aprobado= False
        cat1.save()  
    return redirect ('app:inicio')

def habilitar_ciudadano_view(request):

    recorrer= User.objects.filter(is_superuser=False)
    print(recorrer)
    contextos={
        'ruta':recorrer
    }

    return render (request, 'app/html/habilitar_ciudadano.html',contextos)

def habilitar_ciudadano_post(request):

    info1= request.POST['seleccionar_ind']
    cat1 = User.objects.get(id=info1)
    info2= int (request.POST['seleccionar'])
    print(info2)
    print(cat1.first_name)
    if info2 == 10:        
        print('holi')
        cat1.is_active= True
        cat1.save()
    else:
        print('chao')
        cat1.is_active= False
        cat1.save()  

    return redirect( 'app:inicio')

def aprobar_afiliacion_view(request):
    lista= Afiliacion.objects.filter(aprobado=False)
    individuo= Individuo.objects.all()
    
    lista2= {
        'id':lista,
        'individuo': individuo
    }
    print ("lista2: ", lista2)

    contexto={
        'holi': lista,
        'chao': individuo,
        'individuos':lista2
       
    }
    return render(request,'app/html/aprobarAfiliación.html', contexto)

def aprobar_afiliacion_post(request):

    info1= request.POST['seleccionar_ind']
    cat1= Afiliacion.objects.get(id=info1)
    info2= int (request.POST ['seleccionar'])
   
    if info2==10:
        cat1.aprobado=True
        cat1.save()
    else:
        cat1.aprobado=False
        cat1.save()

    return redirect( 'app:inicio')

def aprobar_proceso_view(request):
    procesos = Proceso.objects.all()
    contexto = {
        'procesos': procesos
    }
    return render(request,'app/html/AprobarProceso.html', contexto)

def aprobar_proceso_post(request):
    id_proceso = request.POST['proceso']
    aprobado = request.POST['aprobado']
    proceso = Proceso.objects.get(id=id_proceso)
    proceso.aprobado = aprobado
    proceso.save()
    return redirect( 'app:inicio')

def implicar_afiliado_view(request):
    afiliados = Afiliacion.objects.filter(aprobado=True)
    procesos = Proceso.objects.filter(aprobado=True)
    contexto = {
        'afiliados':afiliados,
        'procesos':procesos
    }
    return render(request,'app/html/implicarAfiliado.html', contexto)

def implicar_afiliado_post(request):
    id_afiliado = request.POST['afiliado']
    id_proceso = request.POST['proceso']
    fecha_implicacion = request.POST['fecha_implicacion']
    acusacion = request.POST['acusasion']
    veredicto = request.POST['culpable']
    pena = request.POST['pena']
    comentarios = request.POST['comentarios']
    implicacion = Implicado(
        id_afiliado=Afiliacion.objects.get(id=id_afiliado),
        id_proceso=Proceso.objects.get(id=id_proceso),
        fecha=fecha_implicacion,
        acusacion=acusacion,
        culpable=veredicto,
        pena=pena,
        comentarios=comentarios
    )
    implicacion.save()
    return redirect( 'app:inicio')

def afiliar_individuo_view(request):
    recorrer= Individuo.objects.all()
    rpartido = Partido.objects.all()
    # print(recorrer)
    contextos={
        'ruta':recorrer,
        'rutap':rpartido
    }
    print("Almenos aca??")

    return render(request,'app/html/afiliar_individuo.html',contextos)

def afiliar_individuo_form(request):

    if(request.user.is_authenticated):
        user = User.objects.get(id=request.user.id)
        print("Sooooyyy:  ",request.user.is_superuser)
        infoin= int(request.POST['selec_ind'])
        infopa= int(request.POST['selec_part'])
        fechai=request.POST['startDate']
        fechas=request.POST['endDate']
        if(user.is_superuser):
            if fechai == "" and fechas !="":
                afiliar=Afiliacion(id_individuo_id=infoin, id_partido_id=infopa, fecha_salida=fechas, aprobado = False)
                print("Bieeen")
                afiliar.save()
            elif fechas == "" and fechai !="":
                afiliar=Afiliacion(id_individuo_id=infoin, id_partido_id=infopa, fecha_ingreso=fechai, aprobado = False)
                print("Bieeen")
                afiliar.save()
            elif fechas == "" and fechai == "":
                afiliar=Afiliacion(id_individuo_id=infoin, id_partido_id=infopa, aprobado = False)
                print("Bieeen")
                afiliar.save()
            elif fechai != "" and fechas != "":
                afiliar=Afiliacion(id_individuo_id=infoin, id_partido_id=infopa, fecha_ingreso=fechai, fecha_salida=fechas, aprobado = False)
                print("Bieeen")
                afiliar.save()
        else:
            if fechai == "" and fechas !="":
                afiliar=Afiliacion(id_individuo_id=infoin, id_partido_id=infopa, fecha_salida=fechas, aprobado = False)
                print("Malll")
                afiliar.save()
            elif fechas == "" and fechai !="":
                afiliar=Afiliacion(id_individuo_id=infoin, id_partido_id=infopa, fecha_ingreso=fechai, aprobado = False)
                print("Malll")
                afiliar.save()
            elif fechas == "" and fechai == "":
                afiliar=Afiliacion(id_individuo_id=infoin, id_partido_id=infopa, aprobado = False)
                print("Malll")
                afiliar.save()
            elif fechai != "" and fechas != "":
                afiliar=Afiliacion(id_individuo_id=infoin, id_partido_id=infopa, fecha_ingreso=fechai, fecha_salida=fechas, aprobado = False)
                print("Malll")
                afiliar.save()

        return redirect('app:inicio')
    else:
        return redirect('app:principal')



   


def form_registro_view(request):
    usuarios = User.objects.all()
    print(usuarios)
    usuario = request.POST['usuario']
    contraseña = request.POST['contraseña']
    email = request.POST['email']
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']

    for i in usuarios:
        if i.username == usuario:
            print("errorU")
            return render(request, 'app/html/error_registro.html')

        if i.email == email:
            print("errorC")
            return render(request, 'app/html/error_registro.html' )

    user = User()
    user.first_name = nombre
    user.last_name = apellido
    user.username = usuario
    user.email = email
    user.set_password(contraseña)
    print(user)
    user.save()
    
    return redirect('app:inicio_ciudadano')

def error_registro_view(request):
    return render(request,'app/html/error_registro.html')

def cambiar_datos_form(request):
    useractual = request.user.username
    print(useractual)
    lista = User.objects.all()	
    usuario = User.objects.get(username=useractual)
    usuariod = request.POST['usuario1']
    contraseña = request.POST['contraseña']
    email = request.POST['email']
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    if  usuariod != "" :
        for i in lista:
            if i.username == usuariod:
                print("errorU")
                
                return render(request, 'app/html/error_cambiar.html')
        
        usuario.username = usuariod
        print("estoy modificando usuario")
        usuario.save()

    if  contraseña != "" :
        usuario.set_password(contraseña)
        usuario.save()

    if  email != "" :
        for i in lista:
            if i.email == email:
                print("errorC")
                return render(request, 'app/html/error_cambiar.html')
          
        usuario.email = email
        usuario.save()
    if  nombre != "" :
        usuario.first_name = nombre
        usuario.save()

    if  apellido != "" :
        usuario.last_name = apellido
        usuario.save()

    return redirect('app:inicio_ciudadano')